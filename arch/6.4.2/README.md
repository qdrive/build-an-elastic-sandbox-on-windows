# Build an Elastic Sandbox on Windows

This doc will walk through the steps required to build an elastic sandbox on Windows. It will cover the installation and basic configuration of Elasticsearch, Logstash, Kibana, APM-Server, Beats and the additional X-Pack trial features. This doc is to be used as a guideline to familiarise yourself with the applications and how they interact with each other and is not intended for production use. Snippets have been taken from the [official documentation](#official-documentation) and adapted/condensed for convenience. Performance will vary depending on the spec of the PC used.

## Applications

| Name                            | Version       |
| :------------------------------ |:------------- |
| APM-Server                      | 6.4.2         |
| Beats                           | 6.4.2         |
| Elasticsearch                   | 6.4.2         |
| Kibana                          | 6.4.2         |
| Logstash                        | 6.4.2         |

## Contents

+ [Prerequisites](#prerequisites)
+ [Environment](#environment)
+ [Elasticsearch](#elasticsearch)
+ [Logstash](#logstash)
+ [Kibana](#kibana)
+ [APM-Server](#apm-server)
+ [Beats](#beats)
+ [Kibana Index Patterns](#kibana-index-patterns)
+ [Additional Documentation](#additional-documentation)

## Prerequisites

+ Java must be installed and configured with a working **JAVA_HOME** environment variable. This documentation uses the Java [jdk-8u151-windows-x64](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html) development kit.
+ To capture network packets, this document has used [WinPcap](https://www.winpcap.org/install/default.htm)
+ [Visual Studio Code](https://code.visualstudio.com) running with elevated permissions has been used as the IDE.
+ [Powershell](https://docs.microsoft.com/en-us/powershell) version 5.1 has been used to run the build files.

## Environment

All of the required files can be obtained by downloading **build-6.4.2.zip** to a location on your device. The [7-Zip](https://www.7-zip.org) command line exe has been included to improve the process. Explore the contents of the build folder for further information. 

> During the build, 650mb* of compressed files are downloaded and extracted. This usually takes about 10 minutes to fully complete.

Once you have downloaded the build files, run this command to start the process:

``` ps1
~\build> .\init.ps1 -config .\default.json
```

Once the process has completed the environment structure will look like this.

```
> dev
  > elastic-6.4.2-sandbox
    > beat
      > auditbeat
      > filebeat
      > heartbeat
      > metricbeat
      > packetbeat
      > winlogbeat
    > data
      > shakespeare_<VERSION>.json
    > stack
      > apm-server
      > elasticsearch
      > kibana
      > logstash
```

## Elasticsearch

Edit **~\elasticsearch\config\elasticsearch.yml** to enable the X-Pack trial features and to change the **cluster** and **node** names to something more meaningful. 

``` yml
####### Additional Syntax Removed for Brevity #######

# ---------------------------------- Cluster -----------------------------------
#
# Use a descriptive name for your cluster:
#
cluster.name: elastic-6.4.2-sandbox
#
# ------------------------------------ Node ------------------------------------
#
# Use a descriptive name for the node:
#
node.name: node-1
# ---------------------------------- Various -----------------------------------
xpack.ml.enabled: true
xpack.monitoring.collection.enabled: true
xpack.security.enabled: true
xpack.security.audit.enabled: true
xpack.watcher.enabled: true
```

Start Elasticsearch by running this command:

``` ps1
~\elasticsearch\bin> .\elasticsearch
```

A response similar to this will be returned:

```
[2018-10-08T17:45:42,082][INFO ][o.e.n.Node               ] [node-1] started
```

In a new console session, run this command to validate that Elasticsearch endpoint is available:

``` ps1
Invoke-RestMethod -Method GET -Uri http://localhost:9200
```

A sucessful response similar to this will be returned:

```
name         : node-1
cluster_name : elastic-6.4.2-sandbox
cluster_uuid : qXOQC7PQRCqQAsDcYI4XrQ
version      : @{number=6.4.2; build_flavor=default; build_type=zip; build_hash=04711c2; build_date=2018-09-26T13:34:09.098244Z;
               build_snapshot=False; lucene_version=7.4.0; minimum_wire_compatibility_version=5.6.0; minimum_index_compatibility_version=5.0.0}
tagline      : You Know, for Search
```

Activate the X-Pack trial features by running this command:

``` ps1
Invoke-WebRequest -Method POST -UseBasicParsing "http://localhost:9200/_xpack/license/start_trial?acknowledge=true"
```

To allow Elasticsearch, Logstash Kibana and Beats to communicate with each other, we need to create some credentials. Generate some random passwords by running this command:

``` ps1
~\elasticsearch\bin\> .\elasticsearch-setup-passwords auto
```

A response similar to this will be returned:

```
Changed password for user kibana
PASSWORD kibana = <KIBANA-PASSWORD>

Changed password for user logstash_system
PASSWORD logstash_system = <LOGSTASH-PASSWORD>

Changed password for user beats_system
PASSWORD beats_system = <BEATS-PASSWORD>

Changed password for user elastic
PASSWORD elastic = <ELASTIC-PASSWORD>
```

Alternatively, run this command and follow the on-screen prompts to create custom passwords:

``` ps1
~\elasticsearch\bin\> .\elasticsearch-setup-passwords auto interactive
```

Run this command to validate that Elasticsearch is running and the new credentials are accepted:

``` ps1
$cred = "$("<ELASTIC-USERNAME>"):$("<ELASTIC-PASSWORD>")"
$encoded = [System.Convert]::ToBase64String([System.Text.Encoding]::ASCII.GetBytes($cred))
$headers = @{ Authorization = "Basic $encoded" }

Invoke-RestMethod -Method GET -Headers $headers -UseBasicParsing -Uri http://localhost:9200
```

## Logstash

Edit **~\logstash\config\logstash.yml** and enter the Elasticsearch credentials.

``` yml
####### Additional Syntax Removed for Brevity #######

# X-Pack Monitoring
xpack.monitoring.enabled: true
xpack.monitoring.elasticsearch.username: <LOGSTASH-USER>
xpack.monitoring.elasticsearch.password: <LOGSTASH-PASSWORD>
xpack.monitoring.elasticsearch.url: ["http://localhost:9200"]
xpack.monitoring.collection.interval: 10s
xpack.monitoring.collection.pipeline.details.enabled: true
```

Edit **~\logstash\config\pipeleines.yml** to tell Logstash where to look for the configuration files.

``` yml
####### Additional Syntax Removed for Brevity #######

- pipeline.id: beat-pipeline
  queue.type: persisted
  path.config: "<FULL-PATH>\\logstash\\conf\\*.conf"
```

Within the **~\logstash** directory create a new folder named **conf** and create the following files.

```
> logstash
    > conf
        > 100-beat-input.conf
        > 200-beat-filter.conf
        > 300-beat-output.conf
```

Each Beat will be assigned a unique port number from where it will listen to requests on. Edit **~\logstash\conf\100-beat-input.conf** and add the following syntax to configure the ports and tags.

``` ruby
input {
  
    beats {
      id => "beats-input"
      port => 5044
      include_codec_tag => false
    }

}
```
 
Edit **~\logstash\conf\200-beat-filter.conf** and add the following syntax to create additional fields and convert the sample json data.

``` ruby
filter {

    if [beat][name] == "auditbeat" {
        mutate {
            id => "auditbeat-add-field"
            add_field => { "greeting" => "Hello from Auditbeat!" }
        }
    }
        
    if [beat][name] == "filebeat" {
        mutate {
            id => "filebeat-add-field"
            add_field => { "greeting" => "Hello from Filebeat!" }
        }
        if [source] =~ "shakespeare" {
            json {
                id => "shakespeare-json"
                source => "message"
                remove_field => ["message"]
            }
            mutate {
                id => "shakespeare-convert"
                convert => {
                    "line_id" => "integer"
                    "play_name" => "string"
                    "speech_number" => "integer"
                    "line_number" => "string"
                    "speaker" => "string"
                    "text_entry" => "string"
                }
            }
        }
    }

    if [beat][name] == "heartbeat" {
        mutate {
            id => "heartbeat-add-field"
            add_field => { "greeting" => "Hello from Heartbeat!" }
        }
    }

    if [beat][name] == "metricbeat" {
        mutate {
            id => "metricbeat-add-field"
            add_field => { "greeting" => "Hello from Metricbeat!" }
        }
    }

    if [beat][name] == "packetbeat" {
        mutate {
            id => "packetbeat-add-field"
            add_field => { "greeting" => "Hello from Packetbeat!" }
        }
    }

    if [beat][name] == "winlogbeat" {
        mutate {
            id => "winlogbeat-add-field"
            add_field => { "greeting" => "Hello from Winlogbeat!" }
        }
    }

}
```

Edit **~\logstash\conf\300-filter-beat.conf** and add the following syntax to create a conditional index for each beat.

``` ruby
output {
    
    elasticsearch {
        id => "elasticsearch-output"
        index => "%{[@metadata][beat]}-%{[@metadata][version]}-%{+YYYY.MM.dd}"
        hosts => ["http://localhost:9200"]
        user => "<ELASTIC-USERNAME>" 
        password => "<ELASTIC-PASSWORD>"
    }

}
```

Test the Logstash configuration by running this command:

``` ps1
~\logstash\bin> .\logstash -t
```

A validated Logstash configuration will return this message:

```
Configuration OK
[2018-10-08T18:04:38,367][INFO ][logstash.runner          ] Using config.test_and_exit mode. Config Validation Result: OK. Exiting Logstash
```

Start Logstash by running this command:

``` ps1
~\logstash\bin> .\logstash
```

A response similar to this will be returned:

```
[2018-10-08T18:06:16,552][INFO ][org.logstash.beats.Server] Starting server on port: 5044
[2018-10-08T18:06:16,599][INFO ][logstash.inputs.metrics  ] Monitoring License OK
[2018-10-08T18:06:18,122][INFO ][logstash.agent           ] Successfully started Logstash API endpoint {:port=>9600}
```

## Kibana

Edit **~\kibana\config\kibana.yml** and add the Elasticsearch credentials.

``` yml 
####### Additional Syntax Removed for Brevity #######

elasticsearch.username: "<KIBANA-USERNAME>" 
elasticsearch.password: "<KIBANA-PASSWORD>"
```

Start Kibana by running this command:

``` ps1
~\kibana\bin> .\kibana
```

When Kibana is ready, a response similar to this will be returned:

```
log   [17:09:29.866] [info][listening][server][http] Server running at http://localhost:5601
```

## APM-Server

Edit **~\apm-server\apm-server.yml** and add the Elasticsearch credentials.

``` yml 
####### Additional Syntax Removed for Brevity #######

#-------------------------- Elasticsearch output ------------------------------
#output.elasticsearch:
  #hosts: ["localhost:9200"]

#----------------------------- Logstash output ---------------------------------
output.logstash:
  # Boolean flag to enable or disable the output module.
  enabled: false

  # The Logstash hosts
  hosts: ["localhost:5044"]
```

Test the APM-Server configuration by running this command:

``` ps1
~\apm-server> .\apm-server test config
```

A validated APM-Server configuration will return this response:

```
Config OK
```

Load the template into Elasticsearch by running this command:

``` ps1
~\apm-server> .\apm-server setup --template -E 'output.elasticsearch.hosts=["localhost:9200"]' -E 'output.elasticsearch.username="<ELASTIC-USERNAME>"' -E 'output.elasticsearch.password="<ELASTIC-PASSWORD>"'
```
 
A successful load will return this message:

```
Loaded index template
```

Start APM-Server by running this command:

``` ps1
~\apm-server> .\apm-server -e
```

To instrument an application, select **APM** within [Kibana](http://localhost:5601/app/kibana#/home/tutorial/apm?) and follow the on-screen instructions.

## Beats

> To configure Beats, the Elasticsearch service must be running.

### Auditbeat
 
Edit **~\auditbeat\auditbeat.yml** and set the location to where files will be read from. We also need to instruct Auditbeat to use Logstash instead of Elasticsearch and enable monitoring.
 
``` yml
####### Additional Syntax Removed for Brevity #######

#================================ General =====================================
name: "auditbeat"
tags: ["sandbox", "app-tier", "dv"]

#-------------------------- Elasticsearch output ------------------------------
#output.elasticsearch:
  # Array of hosts to connect to.
  #hosts: ["localhost:9200"]

#----------------------------- Logstash output --------------------------------
output.logstash:
  # The Logstash hosts
  hosts: ["localhost:5044"]

#============================== Xpack Monitoring ===============================
xpack.monitoring:
  enabled: true
  elasticsearch:
    hosts: ["http://localhost:9200"]
    username: "<ELASTIC-USERNAME>"
    password: "<ELASTIC-PASSWORD>"
```

Test the Auditbeat configuration by running this command:

``` ps1
~\auditbeat> .\auditbeat test config
```

A validated Auditbeat configuration will return this response:

```
Config OK
```

Load the template into Elasticsearch by running this command:

``` ps1
~\auditbeat> .\auditbeat setup --template -E output.logstash.enabled=false -E 'output.elasticsearch.hosts=["localhost:9200"]' -E 'output.elasticsearch.username="<ELASTIC-USERNAME>"' -E 'output.elasticsearch.password="<ELASTIC-PASSWORD>"'
```
 
A successful load will return this message:

```
Loaded index template
```

Start Auditbeat by running this command:

``` ps1
~\auditbeat> .\auditbeat -e
```

### Filebeat
 
Edit **~\filebeat\filebeat.yml** and set the location to where files will be read from. We also need to instruct Filebeat to use Logstash instead of Elasticsearch and enable monitoring.
 
``` yml
####### Additional Syntax Removed for Brevity #######

#=========================== Filebeat inputs =============================
- type: log

  # Change to true to enable this prospector configuration.
  enabled: true

  # Paths that should be crawled and fetched. Glob based paths.
  paths:
    - <FULL-PATH>\data\*

#================================ General =====================================
name: "filebeat"
tags: ["sandbox", "app-tier", "dv"]

#-------------------------- Elasticsearch output ------------------------------
#output.elasticsearch:
  # Array of hosts to connect to.
  #hosts: ["localhost:9200"]

#----------------------------- Logstash output --------------------------------
output.logstash:
  # The Logstash hosts
  hosts: ["localhost:5044"]

#============================== Xpack Monitoring ===============================
xpack.monitoring:
  enabled: true
  elasticsearch:
    hosts: ["http://localhost:9200"]
    username: "<ELASTIC-USERNAME>"
    password: "<ELASTIC-PASSWORD>"
```

Test the Filebeat configuration by running this command:

``` ps1
~\filebeat> .\filebeat test config
```

A validated Filbeat configuration will return this response:

```
Config OK
```

Load the template into Elasticsearch by running this command:

``` ps1
~\filebeat> .\filebeat setup --template -E output.logstash.enabled=false -E 'output.elasticsearch.hosts=["localhost:9200"]' -E 'output.elasticsearch.username="<ELASTIC-USERNAME>"' -E 'output.elasticsearch.password="<ELASTIC-PASSWORD>"'
```
 
A successful load will return this message:

```
Loaded index template
```

Start Filebeat by running this command:

``` ps1
~\filebeat> .\filebeat -e
```

### Heartbeat
 
Edit **~\heartbeat\heartbeat.yml** and instruct Heartbeat to use Logstash instead of Elasticsearch and enable monitoring.

``` yml
####### Additional Syntax Removed for Brevity #######

############################# Heartbeat ######################################

# Configure monitors
heartbeat.monitors:
- type: http

  # List or urls to query
  urls: ["http://localhost:9200", "https://www.elastic.co", "https://www.google.com", "https://www.microsoft.com"]

#================================ General =====================================
name: "heartbeat"
tags: ["sandbox", "app-tier", "dv"]

#-------------------------- Elasticsearch output ------------------------------
#output.elasticsearch:
  # Array of hosts to connect to.
  #hosts: ["localhost:9200"]

#----------------------------- Logstash output --------------------------------
output.logstash:
  # The Logstash hosts
  hosts: ["localhost:5044"]

#============================== Xpack Monitoring ===============================
xpack.monitoring:
  enabled: true
  elasticsearch:
    hosts: ["http://localhost:9200"]
    username: "<ELASTIC-USERNAME>"
    password: "<ELASTIC-PASSWORD>"
```

Test the Heartbeat configuration by running this command:

``` ps1
~\heartbeat> .\heartbeat test config
```

A validated Heartbeat configuration will return this response:

```
Config OK
```

Load the template into Elasticsearch by running this command:

``` ps1
~\heartbeat> .\heartbeat setup --template -E output.logstash.enabled=false -E 'output.elasticsearch.hosts=["localhost:9200"]' -E 'output.elasticsearch.username="<ELASTIC-USERNAME>"' -E 'output.elasticsearch.password="<ELASTIC-PASSWORD>"'
```

A successful load will return this message:

```
Loaded index template
```

Start Heartbeat by running this command:

``` ps1
~\heartbeat> .\heartbeat -e
```

### Metricbeat
 
Edit **~\metricbeat\metricbeat.yml** and instruct Metricbeat to use Logstash instead of Elasticsearch and enable monitoring.

``` yml
####### Additional Syntax Removed for Brevity #######

#================================ General =====================================
name: "metricbeat"
tags: ["sandbox", "app-tier", "dv"]

#-------------------------- Elasticsearch output ------------------------------
#output.elasticsearch:
  # Array of hosts to connect to.
  #hosts: ["localhost:9200"]

#----------------------------- Logstash output --------------------------------
output.logstash:
  # The Logstash hosts
  hosts: ["localhost:5044"]

#============================== Xpack Monitoring ===============================
xpack.monitoring:
  enabled: true
  elasticsearch:
    hosts: ["http://localhost:9200"]
    username: "<ELASTIC-USERNAME>"
    password: "<ELASTIC-PASSWORD>"
```

Enable the Windows module by running this command:

``` ps1
~\metricbeat> .\metricbeat modules enable windows
```

Test the Metricbeat configuration by running this command:

``` ps1
~\metricbeat> .\metricbeat test config
```

A validated Metricbeat configuration will return this response:

```
Config OK
```

Load the template into Elasticsearch by running this command:

``` ps1
~\metricbeat> .\metricbeat setup --template -E output.logstash.enabled=false -E 'output.elasticsearch.hosts=["localhost:9200"]' -E 'output.elasticsearch.username="<ELASTIC-USERNAME>"' -E 'output.elasticsearch.password="<ELASTIC-PASSWORD>"'
```

A successful load will return this message:

```
Loaded index template
```

Start Metricbeat by running this command:

``` ps1
~\metricbeat> .\metricbeat -e
```

### Packetbeat

To find all of the avaiable devices, run this command:

``` ps1
~\packetbeat> .\packetbeat devices
```

Edit **~\packetbeat\packetbeat.yml** and instruct Packetbeat to use Logstash instead of Elasticsearch and enable monitoring. 

``` yml
####### Additional Syntax Removed for Brevity #######

#============================== Network device ================================
packetbeat.interfaces.device: <DEVICE-ID>

#================================ General =====================================
name: "packetbeat"
tags: ["sandbox", "app-tier", "dv"]

#-------------------------- Elasticsearch output ------------------------------
#output.elasticsearch:
  # Array of hosts to connect to.
  #hosts: ["localhost:9200"]

#----------------------------- Logstash output --------------------------------
output.logstash:
  # The Logstash hosts
  hosts: ["localhost:5044"]

#============================== Xpack Monitoring ===============================
xpack.monitoring:
  enabled: true
  elasticsearch:
    hosts: ["http://localhost:9200"]
    username: "<ELASTIC-USERNAME>"
    password: "<ELASTIC-PASSWORD>"
```

Test the Packetbeat configuration by running this command:

``` ps1
~\packetbeat> .\packetbeat test config
```

A validated Packetbeat configuration will return this response:

```
Config OK
```

Load the template into Elasticsearch by running this command:

``` ps1
~\packetbeat> .\packetbeat setup --template -E output.logstash.enabled=false -E 'output.elasticsearch.hosts=["localhost:9200"]' -E 'output.elasticsearch.username="<ELASTIC-USERNAME>"' -E 'output.elasticsearch.password="<ELASTIC-PASSWORD>"'
```

A successful load will return this message:

```
Loaded index template
```

Start Packetbeat by running this command:

``` ps1
~\packetbeat> .\packetbeat -e
```

### Winlogbeat
 
Edit **~\winlogbeat\winlogbeat.yml** and instruct Winlogbeat to use Logstash instead of Elasticsearch and enable monitoring.

``` yml
####### Additional Syntax Removed for Brevity #######

#================================ General =====================================
name: "winlogbeat"
tags: ["sandbox", "app-tier", "dv"]

#-------------------------- Elasticsearch output ------------------------------
#output.elasticsearch:
  # Array of hosts to connect to.
  #hosts: ["localhost:9200"]

#----------------------------- Logstash output --------------------------------
output.logstash:
  # The Logstash hosts
  hosts: ["localhost:5044"]

#============================== Xpack Monitoring ===============================
xpack.monitoring:
  enabled: true
  elasticsearch:
    hosts: ["http://localhost:9200"]
    username: "<ELASTIC-USERNAME>"
    password: "<ELASTIC-PASSWORD>"
```

Test the Winlogbeat configuration by running this command:

``` ps1
~\winlogbeat> .\winlogbeat test config
```

A validated Winlogbeat configuration will return this response:

```
Config OK
```

Load the template into Elasticsearch by running this command:

``` ps1
~\winlogbeat> .\winlogbeat setup --template -E output.logstash.enabled=false -E 'output.elasticsearch.hosts=["localhost:9200"]' -E 'output.elasticsearch.username="<ELASTIC-USERNAME>"' -E 'output.elasticsearch.password="<ELASTIC-PASSWORD>"'
```

A successful load will return this message:

```
Loaded index template
```

Start Winlogbeat by running this command:

``` ps1
~\winlogbeat> .\winlogbeat -e
```

## Kibana Index Patterns
 
Before we can start interrogating our Beat data, we need to add the index patterns that were defined in the Logstash pipeline conf files.
Open up a browser and navigate to **http://localhost:5601** to access the Kibana instance. Login as an administrator using the `<ELASTIC-USERNAME>` and `<ELASTIC-PASSWORD>` credentials. From the Kibana menu, select **Setup index patterns** to begin creating the indexes. 

### APM-Server

+ Enter `apm-*` in the **Index pattern** textbox and click **Next step**
+ From the **Time Filter field name** listbox, select `@timestamp` then click **Create index pattern**

### Auditbeat

+ Enter `auditbeat-*` in the **Index pattern** textbox and click **Next step**
+ From the **Time Filter field name** listbox, select `@timestamp` then click **Create index pattern**

### Filebeat
 
+ Enter `filebeat-*` in the **Index pattern** textbox and click **Next step**
+ From the **Time Filter field name** listbox, select `@timestamp` then click **Create index pattern**

### Heartbeat
 
+ Enter `heartbeat-*` in the **Index pattern** textbox and click **Next step**
+ From the **Time Filter field name** listbox, select `@timestamp` then click **Create index pattern**
 
### Metricbeat
 
+ Enter `metricbeat-*` in the **Index pattern** textbox and click **Next step**
+ From the **Time Filter field name** listbox, select `@timestamp` then click **Create index pattern**

### Packetbeat
 
+ Enter `packetbeat-*` in the **Index pattern** textbox and click **Next step**
+ From the **Time Filter field name** listbox, select `@timestamp` then click **Create index pattern**

### Winlogbeat
 
+ Enter `winlogbeat-*` in the **Index pattern** textbox and click **Next step**
+ From the **Time Filter field name** listbox, select `@timestamp` then click **Create index pattern**

When all of the index patterns have been created, you are ready to start discovering your data. 

## Additional Documentation

+ [Elastic Stack and Product Documentation](https://www.elastic.co/guide/index.html)