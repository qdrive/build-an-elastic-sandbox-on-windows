# Build an Elastic Sandbox on Windows

This doc will walk through the steps required to build an elastic sandbox on Windows. It will cover the installation and basic configuration of Elasticsearch, Logstash, Kibana, APM-Server, Beats and the additional X-Pack trial features. This doc is to be used as a guideline to familiarise yourself with the applications and how they interact with each other and is not intended for production use. Snippets have been taken from the [official documentation](#additional-documentation) and adapted/condensed for convenience. Performance will vary depending on the spec of the PC used.

## Applications

| Name                            | Version       |
| :------------------------------ |:------------- |
| APM-Server                      | 6.6.1         |
| Beats                           | 6.6.1         |
| Elasticsearch                   | 6.6.1         |
| Kibana                          | 6.6.1         |
| Logstash                        | 6.6.1         |

## Contents

+ [Prerequisites](#prerequisites)
+ [Environment](#environment)
+ [Elasticsearch](#elasticsearch)
+ [Logstash](#logstash)
+ [Kibana](#kibana)
+ [APM-Server](#apm-server)
+ [Beats](#beats)
+ [Kibana Index Patterns](#kibana-index-patterns)
+ [Additional Documentation](#additional-documentation)

## Prerequisites

+ Java must be installed and configured with a working **JAVA_HOME** environment variable. This documentation uses the Java [jdk-8u201-windows-x64](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html) development kit.
+ To capture network packets, this document has used [WinPcap](https://www.winpcap.org/install/default.htm)
+ [Visual Studio Code](https://code.visualstudio.com) running with elevated permissions has been used as the IDE.
+ [Powershell](https://docs.microsoft.com/en-us/powershell) version 5.1 has been used to run the build files.

## Environment

All of the required files can be obtained by downloading **build-6.6.1.zip** to a location on your device. The [7-Zip](https://www.7-zip.org) command line exe has been included to improve the process. Explore the contents of the build folder for further information. 

> During the build, 565mb* of compressed files are downloaded and extracted. This usually takes about 10 minutes to fully complete.

Once you have downloaded the build files, run this command to start the process:

``` ps1
~\build> .\init.ps1 -config .\default.json
```

Once the process has completed the environment structure will look like this.

```
> dev
  > es_6.6.1_sandbox
    > beat
      > auditbeat
      > filebeat
      > heartbeat
      > metricbeat
      > packetbeat
      > winlogbeat
    > data
      > shakespeare_<VERSION>.json
    > stack
      > apm-server
      > elasticsearch
      > kibana
      > logstash
```

## Elasticsearch

Edit **~\elasticsearch\config\elasticsearch.yml** to enable the X-Pack trial features (*Machine Learning, Security, Monitoring and Alerting*) and to change the **cluster** and **node** names to something more meaningful. 

``` yml
####### Additional Syntax Removed for Brevity #######

# ---------------------------------- Cluster -----------------------------------
#
# Use a descriptive name for your cluster:
#
cluster.name: es_6.6.1_sandbox
#
# ------------------------------------ Node ------------------------------------
#
# Use a descriptive name for the node:
#
node.name: node_1
# ---------------------------------- Various -----------------------------------
xpack.ml.enabled: true
xpack.monitoring.enabled: true
xpack.monitoring.collection.enabled: true
xpack.security.enabled: true
xpack.security.audit.enabled: true
xpack.watcher.enabled: true
```

Start Elasticsearch by running this command:

``` ps1
~\elasticsearch\bin> .\elasticsearch
```

A response similar to this will be returned:

```
[2019-03-10T11:04:54,817][INFO ][o.e.n.Node               ] [node-1] started
```

In a new console session, run this command to validate that Elasticsearch endpoint is available:

``` ps1
Invoke-RestMethod -Method GET -Uri http://localhost:9200
```

A sucessful response similar to this will be returned:

```
name         : node-1
cluster_name : es_6.6.1_sandbox
cluster_uuid : t23z3xyAQI6uabQ-GZtXbw
version      : @{number=6.6.1; build_flavor=default; build_type=zip; build_hash=1fd8f69; build_date=2019-02-13T17:10:04.160291Z; build_snapshot=False;
               lucene_version=7.6.0; minimum_wire_compatibility_version=5.6.0; minimum_index_compatibility_version=5.0.0}
tagline      : You Know, for Search
```

Activate the X-Pack trial features by running this command:

``` ps1
Invoke-WebRequest -Method POST -UseBasicParsing "http://localhost:9200/_xpack/license/start_trial?acknowledge=true"
```

A sucessful activation will return a similar response to this:

```
StatusCode        : 200
StatusDescription : OK
Content           : {"acknowledged":true,"trial_was_started":true,"type":"trial"}
RawContent        : HTTP/1.1 200 OK
                    Content-Length: 61
                    Content-Type: application/json; charset=UTF-8

                    {"acknowledged":true,"trial_was_started":true,"type":"trial"}
Forms             :
Headers           : {[Content-Length, 61], [Content-Type, application/json; charset=UTF-8]}
Images            : {}
InputFields       : {}
Links             : {}
ParsedHtml        :
RawContentLength  : 61
```

To allow the stack to communicate securly, we need to create some credentials. Generate some random passwords by running this command:

``` ps1
~\elasticsearch\bin\> .\elasticsearch-setup-passwords auto
```

Alternatively, run this command and follow the on-screen prompts to create custom passwords:

``` ps1
~\elasticsearch\bin\> .\elasticsearch-setup-passwords interactive
```

A response similar to this will be returned:

```
Changed password for user [apm_system]
Changed password for user [kibana]
Changed password for user [logstash_system]
Changed password for user [beats_system]
Changed password for user [remote_monitoring_user]
Changed password for user [elastic]
```

Run this command to validate that Elasticsearch is running and the new credentials are accepted:

``` ps1
$cred = "$("elastic"):$("<PASSWORD>")"
$encoded = [System.Convert]::ToBase64String([System.Text.Encoding]::ASCII.GetBytes($cred))
$headers = @{ Authorization = "Basic $encoded" }

Invoke-RestMethod -Method GET -Headers $headers -UseBasicParsing -Uri http://localhost:9200
```

A validated response will return a message similar to this:

```
name         : node_1
cluster_name : es_6.6.1_sandbox
cluster_uuid : t23z3xyAQI6uabQ-GZtXbw
version      : @{number=6.6.1; build_flavor=default; build_type=zip; build_hash=1fd8f69; build_date=2019-02-13T17:10:04.160291Z; build_snapshot=False;
               lucene_version=7.6.0; minimum_wire_compatibility_version=5.6.0; minimum_index_compatibility_version=5.0.0}
tagline      : You Know, for Search
```

## Logstash

Edit **~\logstash\config\logstash.yml** to enter the Elasticsearch credentials and enable automtic reloading (removes the need to restart Logstash when a configuration item has been changed).

``` yml
####### Additional Syntax Removed for Brevity ######

# ------------ Pipeline Configuration Settings --------------
config.reload.automatic: true
config.reload.interval: 3s

# ------------ X-Pack Settings (not applicable for OSS build)--------------
# X-Pack Monitoring
xpack.monitoring.enabled: true
xpack.monitoring.elasticsearch.username: logstash_system
xpack.monitoring.elasticsearch.password: <PASSWORD>
xpack.monitoring.elasticsearch.url: ["http://localhost:9200"]
xpack.monitoring.collection.interval: 10s
xpack.monitoring.collection.pipeline.details.enabled: true
```

Using [Pipeline to Pipeline Communication](https://www.elastic.co/guide/en/logstash/6.6/pipeline-to-pipeline.html), we can abstract away the **Input**, **Filter** and **Output** elements to make the confiuration more maintainable. This example breaks down each Beat into it's own pipeline. Edit **~\logstash\config\pipeleines.yml** to tell Logstash where to look for the configuration files.

``` yml
####### Additional Syntax Removed for Brevity #######

- pipeline.id: beats_input
  config.string: |
    input { beats { port => 5044 } }
    output {
      if [beat][name] == "apmserver" { pipeline { send_to => apmserver_filter } }
      else  if "auditbeat" in [tags] { pipeline { send_to => auditbeat_filter } }
      else  if "filebeat" in [tags] { pipeline { send_to => filebeat_filter } }
      else  if "heartbeat" in [tags] { pipeline { send_to => heartbeat_filter } }
      else  if "metricbeat" in [tags] { pipeline { send_to => metricbeat_filter } }
      else  if "packetbeat" in [tags] { pipeline { send_to => packetbeat_filter } }
      else  if "winlogbeat" in [tags] { pipeline { send_to => winlogbeat_filter } }
    }

- pipeline.id: apmserver_filter
  path.config: ..\conf\apmserver_filter.conf 

- pipeline.id: auditbeat_filter
  path.config: ..\conf\auditbeat_filter.conf

- pipeline.id: filebeat_filter
  path.config: ..\conf\filebeat_filter.conf

- pipeline.id: heartbeat_filter
  path.config: ..\conf\heartbeat_filter.conf

- pipeline.id: metricbeat_filter
  path.config: ..\conf\metricbeat_filter.conf

- pipeline.id: winlogbeat_filter
  path.config: ..\conf\winlogbeat_filter.conf

- pipeline.id: beat_output
  pipeline.workers: 1
  pipeline.batch.size: 1
  config.string: |
    input { pipeline { address => beat_output } }
    output {
      elasticsearch {
        index => "%{[@metadata][beat]}-%{[@metadata][version]}-%{+YYYY.MM.dd}"
        hosts => ["http://localhost:9200"]
        user => "elastic" 
        password => "<PASSWORD>"
      }
    }
```

Within the **~\logstash** directory create a new folder named **conf** and create the following files.

```
> logstash
    > conf
        > apmserver_filter.conf
        > auditbeat_filter.conf
        > filebeat_filter.conf
        > heartbeat_filter.conf
        > metricbeat_filter.conf
        > packetbeat_filter.conf
        > winlogbeat_filter.conf
```

Edit **~\apmserver_filter.conf** and add the following syntax to include an ID and create an additional field.

``` ruby
input { pipeline { address => apmserver_filter } }

filter {
    mutate {
        id => "apmserver_add_greeting"
        add_field => { "greeting" => "Greetings from APMServer!" }
    }
}

output { pipeline { send_to => beat_output } }
```

Edit **~\auditbeat_filter.conf** and add the following syntax to include an ID and create an additional field.

``` ruby
input { pipeline { address => auditbeat_filter } }

filter {
    mutate {
        id => "auditbeat_add_greeting"
        add_field => { "greeting" => "Greetings from Auditbeat!" }
    }
}

output { pipeline { send_to => beat_output } }
```

Edit **~\filebeat_filter.conf** and add the following syntax to include an ID, create an additional field and extract the sample JSON data into seperate fields.

``` ruby
input { pipeline { address => filebeat_filter } }

filter {
    mutate {
        id => "filebeat_add_greeting"
        add_field => { "greeting" => "Greetings from Filebeat!" }
    }
    if [source] =~ "shakespeare" {
        json {
            source => "message"
            target => "[shakespeare]"
        }
    }
}

output { pipeline { send_to => beat_output } }

```

Edit **~\heartbeat_filter.conf** and add the following syntax to include an ID and create an additional field.

``` ruby
input { pipeline { address => heartbeat_filter } }

filter {
    mutate {
        id => "heartbeat_add_greeting"
        add_field => { "greeting" => "Greetings from Heartbeat!" }
    }
}

output { pipeline { send_to => beat_output } }
```

Edit **~\metricbeat_filter.conf** and add the following syntax to include an ID and create an additional field.

``` ruby
input { pipeline { address => metricbeat_filter } }

filter {
    mutate {
        id => "metricbeat_add_greeting"
        add_field => { "greeting" => "Greetings from Metricbeat!" }
    }
}

output { pipeline { send_to => beat_output } }
```

Edit **~\packetbeat_filter.conf** and add the following syntax to include an ID and create an additional field.

``` ruby
input { pipeline { address => packetbeat_filter } }

filter {
    mutate {
        id => "packetbeat_add_greeting"
        add_field => { "greeting" => "Greetings from Packetbeat!" }
    }
}

output { pipeline { send_to => beat_output } }
```

Edit **~\winlogbeat_filter.conf** and add the following syntax to include an ID and create an additional field.

``` ruby
input { pipeline { address => winlogbeat_filter } }

filter {
    mutate {
        id => "winlogbeat_add_greeting"
        add_field => { "greeting" => "Greetings from Winlogbeat!" }
    }
}

output { pipeline { send_to => beat_output } }
```

Test the Logstash configuration by running this command:

``` ps1
~\logstash\bin> .\logstash -t
```

A validated Logstash configuration will return this message:

```
Configuration OK
[2019-03-10T12:49:11,302][INFO ][logstash.runner          ] Using config.test_and_exit mode. Config Validation Result: OK. Exiting Logstash
```

Start Logstash by running this command:

``` ps1
~\logstash\bin> .\logstash
```

A response similar to this will be returned:

```
[2019-03-10T12:51:03,572][INFO ][logstash.agent           ] Successfully started Logstash API endpoint {:port=>9600}
```

## Kibana

Edit **~\kibana\config\kibana.yml** and add the Elasticsearch credentials.

``` yml 
####### Additional Syntax Removed for Brevity #######

elasticsearch.username: "kibana" 
elasticsearch.password: "<PASSWORD>"
```

Start Kibana by running this command:

``` ps1
~\kibana\bin> .\kibana
```

When Kibana is ready, a response similar to this will be returned:

```
log   [12:57:00.125] [info][listening] Server running at http://localhost:5601
```

## APM-Server

Edit **~\apm-server\apm-server.yml** and add the Elasticsearch credentials.

``` yml 
####### Additional Syntax Removed for Brevity #######

#-------------------------- Elasticsearch output ------------------------------
#output.elasticsearch:
  #hosts: ["localhost:9200"]

#----------------------------- Logstash output ---------------------------------
output.logstash:
  # Boolean flag to enable or disable the output module.
  enabled: true

  # The Logstash hosts
  hosts: ["localhost:5044"]

#============================== Xpack Monitoring ===============================
xpack.monitoring:
  enabled: true
  elasticsearch:
    hosts: ["http://localhost:9200"]
    username: "apm_system>"
    password: "<PASSWORD>"
```

Test the APM-Server configuration by running this command:

``` ps1
~\apm-server> .\apm-server test config
```

A validated APM-Server configuration will return this response:

```
Config OK
```

Load the template into Elasticsearch by running this command:

``` ps1
~\apm-server> .\apm-server setup --template -E output.logstash.enabled=false -E 'output.elasticsearch.hosts=["localhost:9200"]' -E 'output.elasticsearch.username="elastic"' -E 'output.elasticsearch.password="<PASSWORD>"'
```
 
A successful load will return this message:

```
Loaded index template
```

Start APM-Server by running this command:

``` ps1
~\apm-server> .\apm-server -e
```

To instrument an application, select **APM** within [Kibana](http://localhost:5601/app/kibana#/home/tutorial/apm?) and follow the on-screen instructions.

## Beats

> To configure Beats, the Elasticsearch service must be running.

### Auditbeat
 
Edit **~\auditbeat\auditbeat.yml** to add additional fields, enable monitoring and instruct Auditbeat to use Logstash instead of Elasticsearch.
 
``` yml
####### Additional Syntax Removed for Brevity #######

#================================ General =====================================
tags: ["auditbeat"]

fields:
 env: sandbox
 tier: app

#-------------------------- Elasticsearch output ------------------------------
#output.elasticsearch:
  # Array of hosts to connect to.
  #hosts: ["localhost:9200"]

#----------------------------- Logstash output --------------------------------
output.logstash:
  # The Logstash hosts
  hosts: ["localhost:5044"]

#============================== Xpack Monitoring ===============================
xpack.monitoring:
  enabled: true
  elasticsearch:
    hosts: ["http://localhost:9200"]
    username: "beats_system"
    password: "<PASSWORD>"
```

Test the Auditbeat configuration by running this command:

``` ps1
~\auditbeat> .\auditbeat test config
```

A validated Auditbeat configuration will return this response:

```
Config OK
```

Load the template into Elasticsearch by running this command:

``` ps1
~\auditbeat> .\auditbeat setup --template -E output.logstash.enabled=false -E 'output.elasticsearch.hosts=["localhost:9200"]' -E 'output.elasticsearch.username="elastic"' -E 'output.elasticsearch.password="<PASSWORD>"'
```
 
A successful load will return this message:

```
Loaded index template
```

Start Auditbeat by running this command:

``` ps1
~\auditbeat> .\auditbeat -e
```

### Filebeat
 
Edit **~\filebeat\filebeat.yml** to add additional fields, enable monitoring and instruct Auditbeat to use Logstash instead of Elasticsearch. We are also going to abstract the configuration elements into a sperate file to make it more maintainable.
 
``` yml
####### Additional Syntax Removed for Brevity #######

#=========================== Filebeat inputs =============================
filebeat.config:
  inputs:
    enabled: true
    path: conf\*.yml
    reload.enabled: true
    reload.period: 10s

#============================= Filebeat modules ===============================

  # Set to true to enable config reloading
  reload.enabled: true

#================================ General =====================================
tags: ["filebeat"]

fields:
 env: sandbox
 tier: app

#-------------------------- Elasticsearch output ------------------------------
#output.elasticsearch:
  # Array of hosts to connect to.
  #hosts: ["localhost:9200"]

#----------------------------- Logstash output --------------------------------
output.logstash:
  # The Logstash hosts
  hosts: ["localhost:5044"]

#============================== Xpack Monitoring ===============================
xpack.monitoring:
  enabled: true
  elasticsearch:
    hosts: ["http://localhost:9200"]
    username: "beats_system"
    password: "<PASSWORD>"
```

Within the **~\Filebeat** directory create a new folder named **conf** and create the following files.

```
> filebeat
    > conf
        > shakespeare_config.yml
```

Edit **~\shakespeare_config.yml** and add the following syntax to include a path to search data/logs in.

``` yml
- type: log
  enabled: true
  paths:
    - <FULL-PATH>\data\*.*
```

Test the Filebeat configuration by running this command:

``` ps1
~\filebeat> .\filebeat test config
```

A validated Filbeat configuration will return this response:

```
Config OK
```

Load the template into Elasticsearch by running this command:

``` ps1
~\filebeat> .\filebeat setup --template -E output.logstash.enabled=false -E 'output.elasticsearch.hosts=["localhost:9200"]' -E 'output.elasticsearch.username="elastic"' -E 'output.elasticsearch.password="<PASSWORD>"'
```
 
A successful load will return this message:

```
Loaded index template
```

Elasticsearch is pretty smart when it decides how data should look when it is ingested in to the system, but there will be times when we need to absolutely declare these manually. As an example, running this command, will declare the field types for the sample Shakespeare dataset:

``` ps1
$cred = "$("elastic"):$("<PASSWORD>")"
$encoded = [System.Convert]::ToBase64String([System.Text.Encoding]::ASCII.GetBytes($cred))

$headers = @{ 
    Authorization = "Basic $encoded" 
    "Content-Type"="application/json"
}

$conf = '{
          "index_patterns": [ "filebeat-*" ],
          "mappings": {
            "doc": {
              "properties": {
                "shakespeare": {
                  "properties": {
                    "type": { "type": "keyword" },
                    "line_id": { "type": "integer" },
                    "play_name": { "type": "keyword" },
                    "speech_number": { "type": "integer" },
                    "speaker": { "type": "keyword" },
                    "text_entry": { "type": "keyword" }
                  }
                }
              }
            }
          }
        }' 

Invoke-RestMethod -Method PUT -Headers $headers -Body $conf -UseBasicParsing -Uri http://localhost:9200/_template/shakespeare_template
```

To verify the newly created mapping template, run this command to return all templates:

``` ps1
$cred = "$("elastic"):$("<PASSWORD>")"
$encoded = [System.Convert]::ToBase64String([System.Text.Encoding]::ASCII.GetBytes($cred))

$headers = @{ 
    Authorization = "Basic $encoded" 
    "Content-Type"="application/json"
}

Invoke-RestMethod -Method GET -Headers $headers -UseBasicParsing -Uri http://localhost:9200/_cat/templates
```

Start Filebeat by running this command:

``` ps1
~\filebeat> .\filebeat -e
```

### Heartbeat
 
Edit **~\heartbeat\heartbeat.yml** and instruct Heartbeat to use Logstash instead of Elasticsearch and enable monitoring.

``` yml
####### Additional Syntax Removed for Brevity #######

############################# Heartbeat ######################################

# Configure monitors
heartbeat.monitors:
- type: http

  # List or urls to query
  urls: ["http://localhost:9200", "https://www.elastic.co", "https://www.google.com", "https://www.microsoft.com"]

#================================ General =====================================
tags: ["heartbeat"]

fields:
 env: sandbox
 tier: app

#-------------------------- Elasticsearch output ------------------------------
#output.elasticsearch:
  # Array of hosts to connect to.
  #hosts: ["localhost:9200"]

#----------------------------- Logstash output --------------------------------
output.logstash:
  # The Logstash hosts
  hosts: ["localhost:5044"]

#============================== Xpack Monitoring ===============================
xpack.monitoring:
  enabled: true
  elasticsearch:
    hosts: ["http://localhost:9200"]
    username: "beats_system"
    password: "<PASSWORD>"
```

Test the Heartbeat configuration by running this command:

``` ps1
~\heartbeat> .\heartbeat test config
```

A validated Heartbeat configuration will return this response:

```
Config OK
```

Load the template into Elasticsearch by running this command:

``` ps1
~\heartbeat> .\heartbeat setup --template -E output.logstash.enabled=false -E 'output.elasticsearch.hosts=["localhost:9200"]' -E 'output.elasticsearch.username="elastic"' -E 'output.elasticsearch.password="<PASSWORD>"'
```

A successful load will return this message:

```
Loaded index template
```

Start Heartbeat by running this command:

``` ps1
~\heartbeat> .\heartbeat -e
```

### Metricbeat
 
Edit **~\metricbeat\metricbeat.yml** and instruct Metricbeat to use Logstash instead of Elasticsearch and enable monitoring.

``` yml
####### Additional Syntax Removed for Brevity #######

#==========================  Modules configuration ============================

metricbeat.config.modules:

  # Set to true to enable config reloading
  reload.enabled: true

#================================ General =====================================
tags: ["metricbeat"]

fields:
 env: sandbox
 tier: app

#-------------------------- Elasticsearch output ------------------------------
#output.elasticsearch:
  # Array of hosts to connect to.
  #hosts: ["localhost:9200"]

#----------------------------- Logstash output --------------------------------
output.logstash:
  # The Logstash hosts
  hosts: ["localhost:5044"]

#============================== Xpack Monitoring ===============================
xpack.monitoring:
  enabled: true
  elasticsearch:
    hosts: ["http://localhost:9200"]
    username: "beats_system"
    password: "<PASSWORD>"
```

Enable the Windows module by running this command:

``` ps1
~\metricbeat> .\metricbeat modules enable windows
```

Test the Metricbeat configuration by running this command:

``` ps1
~\metricbeat> .\metricbeat test config
```

A validated Metricbeat configuration will return this response:

```
Config OK
```

Load the template into Elasticsearch by running this command:

``` ps1
~\metricbeat> .\metricbeat setup --template -E output.logstash.enabled=false -E 'output.elasticsearch.hosts=["localhost:9200"]' -E 'output.elasticsearch.username="elastic"' -E 'output.elasticsearch.password="<PASSWORD>"'
```

A successful load will return this message:

```
Loaded index template
```

Start Metricbeat by running this command:

``` ps1
~\metricbeat> .\metricbeat -e
```

### Packetbeat

To find all of the avaiable devices, run this command:

``` ps1
~\packetbeat> .\packetbeat devices
```

Edit **~\packetbeat\packetbeat.yml** and instruct Packetbeat to use Logstash instead of Elasticsearch and enable monitoring. 

``` yml
####### Additional Syntax Removed for Brevity #######

#============================== Network device ================================
packetbeat.interfaces.device: <DEVICE-ID>

#================================ General =====================================
tags: ["packetbeat"]

fields:
 env: sandbox
 tier: app

#-------------------------- Elasticsearch output ------------------------------
#output.elasticsearch:
  # Array of hosts to connect to.
  #hosts: ["localhost:9200"]

#----------------------------- Logstash output --------------------------------
output.logstash:
  # The Logstash hosts
  hosts: ["localhost:5044"]

#============================== Xpack Monitoring ===============================
xpack.monitoring:
  enabled: true
  elasticsearch:
    hosts: ["http://localhost:9200"]
    username: "beats_system"
    password: "<PASSWORD>"
```

Test the Packetbeat configuration by running this command:

``` ps1
~\packetbeat> .\packetbeat test config
```

A validated Packetbeat configuration will return this response:

```
Config OK
```

Load the template into Elasticsearch by running this command:

``` ps1
~\packetbeat> .\packetbeat setup --template -E output.logstash.enabled=false -E 'output.elasticsearch.hosts=["localhost:9200"]' -E 'output.elasticsearch.username="elastic"' -E 'output.elasticsearch.password="<PASSWORD>"'
```

A successful load will return this message:

```
Loaded index template
```

Start Packetbeat by running this command:

``` ps1
~\packetbeat> .\packetbeat -e
```

### Winlogbeat
 
Edit **~\winlogbeat\winlogbeat.yml** and instruct Winlogbeat to use Logstash instead of Elasticsearch and enable monitoring.

``` yml
####### Additional Syntax Removed for Brevity #######

#================================ General =====================================
tags: ["winlogbeat"]

fields:
 env: sandbox
 tier: app

#-------------------------- Elasticsearch output ------------------------------
#output.elasticsearch:
  # Array of hosts to connect to.
  #hosts: ["localhost:9200"]

#----------------------------- Logstash output --------------------------------
output.logstash:
  # The Logstash hosts
  hosts: ["localhost:5044"]

#============================== Xpack Monitoring ===============================
xpack.monitoring:
  enabled: true
  elasticsearch:
    hosts: ["http://localhost:9200"]
    username: "beats_system"
    password: "<PASSWORD>"
```

Test the Winlogbeat configuration by running this command:

``` ps1
~\winlogbeat> .\winlogbeat test config
```

A validated Winlogbeat configuration will return this response:

```
Config OK
```

Load the template into Elasticsearch by running this command:

``` ps1
~\winlogbeat> .\winlogbeat setup --template -E output.logstash.enabled=false -E 'output.elasticsearch.hosts=["localhost:9200"]' -E 'output.elasticsearch.username="elastic"' -E 'output.elasticsearch.password="<PASSWORD>"'
```

A successful load will return this message:

```
Loaded index template
```

Start Winlogbeat by running this command:

``` ps1
~\winlogbeat> .\winlogbeat -e
```

## Kibana Index Patterns
 
Before we can start interrogating our Beat data, we need to add the index patterns that were defined in the Logstash pipeline conf files.
Open up a browser and navigate to **http://localhost:5601** to access the Kibana instance. Login as an administrator using the `elastic` user credentials. From the Kibana menu, select **Setup index patterns** to begin creating the indexes. 

### APM-Server

+ Enter `apm-*` in the **Index pattern** textbox and click **Next step**
+ From the **Time Filter field name** listbox, select `@timestamp` then click **Create index pattern**

### Auditbeat

+ Enter `auditbeat-*` in the **Index pattern** textbox and click **Next step**
+ From the **Time Filter field name** listbox, select `@timestamp` then click **Create index pattern**

### Filebeat
 
+ Enter `filebeat-*` in the **Index pattern** textbox and click **Next step**
+ From the **Time Filter field name** listbox, select `@timestamp` then click **Create index pattern**

### Heartbeat
 
+ Enter `heartbeat-*` in the **Index pattern** textbox and click **Next step**
+ From the **Time Filter field name** listbox, select `@timestamp` then click **Create index pattern**
 
### Metricbeat
 
+ Enter `metricbeat-*` in the **Index pattern** textbox and click **Next step**
+ From the **Time Filter field name** listbox, select `@timestamp` then click **Create index pattern**

### Packetbeat
 
+ Enter `packetbeat-*` in the **Index pattern** textbox and click **Next step**
+ From the **Time Filter field name** listbox, select `@timestamp` then click **Create index pattern**

### Winlogbeat
 
+ Enter `winlogbeat-*` in the **Index pattern** textbox and click **Next step**
+ From the **Time Filter field name** listbox, select `@timestamp` then click **Create index pattern**

When all of the index patterns have been created, you are ready to start discovering your data. 

## Additional Documentation

+ [Elastic Stack and Product Documentation](https://www.elastic.co/guide/index.html)